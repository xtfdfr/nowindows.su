# nowindows.su
The source code of the [nowindows.su](https://nowindows.su) site. If you can translate this site to a new language or improve something, please open a merge request.